﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repricer
{
    class NewPriceCollection
    {

          static MongoCollection<NewPrice> collection;

          public NewPriceCollection()
        {
            MongoClient Client = new MongoClient("mongodb://client148:client148devnetworks@" + Properties.Settings.Default.DatabaseIP + @"/RepricerCollection");
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("RepricerCollection");
            collection = Database.GetCollection<NewPrice>("newbookprice");

        }

        public NewPrice GetDocumentByAsin(string asin)
        {
            return collection.FindOne(Query.EQ("asin", asin));
        }

        public NewPrice GetDocumentBySku(string sku)
        {
            return collection.FindOne(Query.EQ("sku", sku));
        }

        //public void SaveToDatabase(List<NewPrice> entities)
        //{
        //    foreach (NewPrice entity in entities)
        //    {
        //        NewPrice temp = GetDocumentByAsin(entity.asin);
        //        if (temp != null)
        //        {
        //            //update
        //            Console.WriteLine("Updating " + entity.asin);
        //            temp.sku = entity.sku;
        //            temp.asin = entity.asin;
        //            temp.price = entity.price;
        //        }
        //        else
        //        {
        //            //add 
        //            Console.WriteLine("Saving " + entity.asin);
        //            try
        //            {
        //                collection.Insert(entity);
        //            }
        //            catch { }

        //        }

        //    }

        //}
    }
}
