﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repricer
{
    class MongoCollections
    {
        static MongoCollection<RepricerEntity> collection;
   
        public MongoCollections(){
            MongoClient Client = new MongoClient("mongodb://client148:client148devnetworks@" + Properties.Settings.Default.DatabaseIP + @"/RepricerCollection");
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("RepricerCollection");
            collection = Database.GetCollection<RepricerEntity>("repricer");

    
        }

       

        private  RepricerEntity GetDocumentByAsin(string asin)
        {
           
                return collection.FindOne(Query.EQ("asin", asin));
     
        }

        private  RepricerEntity GetDocumentBySku(string sku)
        {
            return collection.FindOne(Query.EQ("sku", sku));
        }
    
        public void SaveToDatabase(List<RepricerEntity> entities) {
            foreach (RepricerEntity entity in entities) {
               
                
                RepricerEntity temp = GetDocumentByAsin(entity.asin);
                if (temp != null) { 
                    //update
                    Console.WriteLine("Updating " + entity.asin);
                    temp.subcondition = entity.subcondition;
                    temp.listingprice = entity.listingprice;
                    temp.update_time = entity.update_time;
                    temp.otherSellerListingPrice = entity.otherSellerListingPrice;
                    temp.percentDifference = entity.percentDifference;
                    collection.Save(temp);
                }
                else { 
                    //add 
                    Console.WriteLine("Saving " + entity.asin);
                    collection.Insert(entity);
       
                }

            }
        
        }
    }

}
