﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repricer
{
    class RepricerEntity
    {
        public ObjectId id { get; set; }
        public string sku { get; set; }
        public string asin { get; set; }
        public string condition { get; set; }
        public string subcondition { get; set; }
        public double listingprice { get; set; }
        public double otherSellerListingPrice { get; set; }
        public double percentDifference { get; set; }
        public DateTime update_time { get; set; }
    }
}
