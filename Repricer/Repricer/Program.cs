﻿using MarketplaceWebService;
using MarketplaceWebService.Model;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using AmazonAPI.Amazon.src.Model;
using AmazonAPI;
using System.Text.RegularExpressions;
using System.Globalization;
using DN_Classes.Agent;
using DN_Classes.AppStatus;
using ProcuctDB.JTL;

namespace Repricer
{
    class Program
    {
        static System.IO.StreamWriter file = new System.IO.StreamWriter(Properties.Settings.Default.logs, true);
        static string logs = "";
        static StringBuilder log = new StringBuilder();
      //  static List<string> NewPrefixes = new List<string>();

        static List<RepricerEntity> entities = new List<RepricerEntity>();
        static string condition = "";
        static bool restartProgram = false;
        static List<Product> ToBeRepriced = new List<Product>();
        static List<Product> ToBeRepricedNew = new List<Product>();
        static NewPriceCollection n = new NewPriceCollection();

        static void Main(string[] args)
        {
            ApplicationStatus s=new ApplicationStatus("BookRepricer_Vitalino");
           s.Start();
            try
            {

                file.WriteLine("*************************************************************************************************");
                file.WriteLine("*************************************************************************************************");
                file.WriteLine("*Date started:" + DateTime.Now.ToString());

                log.Append("SKU\tOur_ListingPrice\tSubCondition\tOther_Seller_SubCondition\tOther_Seller_Listing_Price\tProposed_Listing_Price\tRemarks\n");

                //1. Get all the Needed Prefix to be Repriced.
                Console.WriteLine("Step1. Get all the Needed Prefix to be Repriced.");

                file.WriteLine("Getting all the Needed Prefix to be Repriced");

                //2. Get Francesco Credentials from MongoDB   
                Console.WriteLine("Step2. Get vitalino Credentials from MongoDB.");
                //   try
                //   {

                InitializeCredentials();

                //3. Download the Inventory File
                Console.WriteLine("Step3. Downloading Inventory File.");
                AmazonUpdater amazon = new AmazonUpdater(Properties.Settings.Default.merchantId,
                                                            Properties.Settings.Default.marketplaceId,
                                                            Properties.Settings.Default.accessKeyId,
                                                            Properties.Settings.Default.secretAccessKey);
                try
                {
                    file.WriteLine("Downloading Inventory File");
                 //   amazon.downloadInventoryFile();
                    file.WriteLine("Inventory File Downloaded");

                }
                catch (Exception exception)
                {
                    file.WriteLine("There was an exception thrown");
                    file.WriteLine("\t" + exception.Message);
                    file.WriteLine("*Date ended:" + DateTime.Now.ToString());
                    file.WriteLine("************************END OF LOG**************************************************************");
                    file.WriteLine("*************************************************************************************************");
                    //  file.Close();
                }
                //4. Narrow down the list using the Prefix. Save the list to the ToBeRepriced List
                Console.WriteLine("Step4. Narrow down the list using the Prefix. Save the list to the ToBeRepriced List.");
                GetToBeRepricedFromInventoryFile();


                //5. Reprice each product in the List
                Console.WriteLine("Step5. Reprice each product in the List");
                Console.WriteLine("SKU  Our_ListingPrice  SubCondition  Other_Seller_SubCondition  Other_Seller_Listing_Price  Proposed_Listing_Price  Remarks");
                RepriceEachProductInTheList(ToBeRepriced, ToBeRepricedNew);


                //add to XML
                //6. Save result to Collection
                Console.WriteLine("Step6. Saving to MongoDB");
                MongoCollections mongo = new MongoCollections();
             //   mongo.SaveToDatabase(entities);

                //7. Send Feeds to Amazon
                Console.WriteLine("Step7. Sending feeds to Amazon");
                try
                {
                //   amazon.SendFeeds();
                }
                catch (Exception exception)
                {
                    file.WriteLine(amazon.getLogs());
                    file.WriteLine("There was an exception thrown");
                    file.WriteLine("\t" + exception.Message);
                    file.WriteLine("*Date ended:" + DateTime.Now.ToString());
                    file.WriteLine("************************END OF LOG**************************************************************");
                    file.WriteLine("*************************************************************************************************");
                    //file.Close();
                }

                // ApplicationStatus.Status s = new ApplicationStatus.Status("AmazonRepricer", DateTime.Now);
                file.WriteLine("*Date ended:" + DateTime.Now.ToString());
                file.WriteLine("************************END OF LOG**************************************************************");
                file.WriteLine("*************************************************************************************************");
                file.Close();
                Console.WriteLine("Done!");

                s.Successful(); 
                s.Stop();
                s.Dispose();
            }
            catch(Exception ex)
            {
                s.Stop();
                s.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                s.Dispose();
            }
        
                // Console.Read(); 
            //}
            //catch (Exception exception)
            //{
            //    //    file.WriteLine("There was an exception thrown");
            //        Console.WriteLine("\t" + exception.Message);
            //    //    file.WriteLine("*Date ended:" + DateTime.Now.ToString());
            //    //    file.WriteLine("************************END OF LOG**************************************************************");
            //    //    file.WriteLine("*************************************************************************************************");
            //    //  //  file.Close();
            //}
        }


        private static void InitializeCredentials()
        {

            try
            {
                Agent agent = new Agent();
                AgentEntity entity = agent.agentEntity;


                Properties.Settings.Default.accessKeyId = entity.agent.keys.amazon.accesskeyid;
                Properties.Settings.Default.secretAccessKey = entity.agent.keys.amazon.secretaccesskeyid;
                Properties.Settings.Default.merchantId = entity.agent.keys.amazon.merchantid;
                Properties.Settings.Default.marketplaceId = entity.agent.keys.amazon.marketplaceid;

            }
            catch
            {
                Properties.Settings.Default.accessKeyId = "";
                Properties.Settings.Default.secretAccessKey = "";
                Properties.Settings.Default.merchantId = "";
                Properties.Settings.Default.marketplaceId = "";
            }
              
            

        }
        public static bool MatchRegex(string s)
        {
            Regex regex = new Regex(@"A\d*-.*");
            Match match = regex.Match(s);
            if (match.Success)
                return true;
            else
                return false;
        }

        private static void GetToBeRepricedFromInventoryFile()
        {
            FileInfo file = new FileInfo(Properties.Settings.Default.InventoryReport);
            string filetext = file.OpenText().ReadToEnd();
            string[] lines = filetext.Replace("\r", "").Split('\n');
            string sku;
            string asin;
            double price;
            int qty = 0;
            string subcondition;
            int count = 0;
            foreach (string line in lines)
            {
                Console.WriteLine(line + "\t" + MatchRegex(line));
                if (!line.Equals(""))
                {

                    try
                    {
                        sku = line.Split('\t')[0];
                        asin = line.Split('\t')[1];
                        try
                        {
                            qty = Convert.ToInt32(line.Split('\t')[3]);
                            //   Console.WriteLine(line);
                        }
                        catch
                        {
                            qty = 0;
                        }


                    //    if (MatchRegex(sku) && sku.StartsWith("A") && qty > 0)
                        if (IsFlexible(sku))
                        {
                            //    Console.WriteLine(line);

                            count++;
                            subcondition = GetProductSubconditionFromAmazon(sku).ToLower();
                            price = Convert.ToDouble(line.Split('\t')[2].ToString(), CultureInfo.InvariantCulture);

                            if (!subcondition.Contains("not included because"))
                            {
                                ToBeRepriced.Add(new Product(sku, asin, subcondition, price, true));
                                Console.WriteLine("\t[" + count + "] " + line + "\t" + subcondition);
                            }
                            else
                            {
                                Console.WriteLine(sku + "  [" + count + "] " + line + "\t" + subcondition);
                                log.Append(sku + "\t" + price + "\t" + subcondition + "\t\t\t\tNot Repriced since Condition is not USED\n");
                            }

                        }

                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex.Message);
                        log.Append(ex.Message);
                        throw;

                    }
                }
            }
        }

        

        private static JTLDB jtlDb=new JTLDB();

        private static bool IsFlexible(string sku)
        {
            try
            {
                JTLEntity entity = (JTLEntity) jtlDb.GetMongoProductByArtnr(sku);
            if (entity.product.pricetype == "04")
                return false;
            else 
                return true;
            }
            catch
            {
                return false;
            }
         
        }

        public static string GetProductSubconditionFromAmazon(string SKU)
        {
            XmlDocument xml = new XmlDocument();
            IMWSResponse response;
            AmazonDetailsGetter amazon;
            do
            {
                amazon = new AmazonDetailsGetter();
                response = amazon.InvokeGetMyPriceForSKU(SKU);
                xml.LoadXml(RemoveAllXmlNamespace(response.ToXML()));
                if (response.ToXML().ToString().Contains("throttle"))
                {
                    Console.WriteLine("Sleeping for 15seconds.Throttled");
                    System.Threading.Thread.Sleep(15000);
                }
            } while (response.ToXML().ToString().Contains("throttle"));

            string data = xml.SelectSingleNode("//Offers/Offer/ItemCondition").InnerText.ToLower();
            if (data.ToLower().Equals("used") || data.ToLower().Equals("new"))
                return xml.SelectSingleNode("//Offers/Offer/ItemSubCondition").InnerText.ToLower();
            else
            {
                return "not included because (" + data + ")";
            }
        }


        private static void RepriceEachProductInTheList(List<Product> products, List<Product> newProducts)
        {
            double newPrice = 0;
            XMLEnvelope envelope = new XMLEnvelope();
            envelope.AddHeader();
            Console.WriteLine("Products:" + products.Count);
            foreach (Product product in products)
            {
                Product p = RepriceThisProduct(product);
                try
                {
                    newPrice = n.GetDocumentBySku(p.SKU).price;
                }
                catch
                {
                    newPrice = double.MaxValue;
                }

                Console.WriteLine("\t\t\t" + p.NewListingPrice() + ">" + newPrice + "=" + (p.NewListingPrice() > newPrice));

                if (p.NewListingPrice() > newPrice)
                {
                    newPrice = newPrice - .01;
                    p.Remarks = "Price is greater than NEW. Underbidding by 0.01";
                }
                else
                    newPrice = p.NewListingPrice();


                log.Append(p.SKU + "\t" + (p.OriginalLandedPrice - 3) + "\t" + p.Subcondition + "\t" + p.OtherSellerSubcondition + "\t" + (p.OtherSellerLandedPrice - 3) + "\t" + Math.Round(newPrice, 2) + "\t" + p.Remarks + "\n");
                Console.WriteLine(p.SKU + "\t" + (p.OriginalLandedPrice - 3) + "\t" + p.Subcondition + "\t" + p.OtherSellerSubcondition + "\t" + (p.OtherSellerLandedPrice - 3) + "\t" + Math.Round(newPrice, 2) + "\t" + p.Remarks + "\n");
                double os = p.OtherSellerLandedPrice >= double.MaxValue ? 0 : p.OtherSellerLandedPrice - 3;
                double pd = p.OtherSellerLandedPrice >= double.MaxValue ? 0 : (newPrice - (p.OtherSellerLandedPrice - 3)) / newPrice * 100;
                entities.Add(new RepricerEntity
                {
                    asin = p.ASIN,
                    condition = "used",
                    listingprice = newPrice,
                    sku = p.SKU,
                    subcondition = p.Subcondition,
                    otherSellerListingPrice = os,
                    percentDifference = pd,
                    update_time = DateTime.Now
                });
                envelope.AddNewNode(p.SKU, newPrice);
            }

            Console.WriteLine("NewProducts:" + newProducts.Count);

            //foreach (Product product in newProducts)
            //{
            //    Product p = RepriceThisNewProduct(product);

            //    log.Append(p.SKU + "\t" + (p.OriginalLandedPrice - 3) + "\t" + p.Subcondition + "\t" + p.OtherSellerSubcondition + "\t" + (p.OtherSellerLandedPrice - 3) + "\t" + Math.Round(p.NewListingPrice(), 2) + "\t" + p.Remarks + "\n");
            //    Console.WriteLine(p.SKU + "\t" + (p.OriginalLandedPrice - 3) + "\t" + p.Subcondition + "\t" + p.OtherSellerSubcondition + "\t" + (p.OtherSellerLandedPrice - 3) + "\t" + Math.Round(p.NewListingPrice(), 2) + "\t" + p.Remarks + "\n");
            //    double os = p.OtherSellerLandedPrice >= double.MaxValue ? 0 : p.OtherSellerLandedPrice - 3;
            //    double pd = p.OtherSellerLandedPrice >= double.MaxValue ? 0 : (newPrice - (p.OtherSellerLandedPrice - 3)) / newPrice * 100;

            //    entities.Add(new RepricerEntity
            //    {
            //        asin = p.ASIN,
            //        condition = "used",
            //        listingprice = newPrice,
            //        sku = p.SKU,
            //        subcondition = p.Subcondition,
            //        otherSellerListingPrice = os,
            //        percentDifference = pd,
            //        update_time = DateTime.Now
            //    });
            //    envelope.AddNewNode(p.SKU, p.NewListingPrice());
            //}
            envelope.SaveXML();
            //save log file
            File.WriteAllText(@"C:\Logs\" + DateTime.Now.ToString("yyyyMMddHHmm") + ".txt", log.ToString());

        }

        public static double lowestPriceBasedonSubcondition(XmlDocument xml, string subcondition)
        {
            double lowestPrice = double.MaxValue;
            var listings = xml.SelectNodes("//LowestOfferListing");
            string OtherSellerSubcondition = "not found";
            foreach (XmlNode listing in listings)
            {
                OtherSellerSubcondition = listing.SelectSingleNode("Qualifiers/ItemSubcondition").InnerText.ToLower();
                string landedprice = listing.SelectSingleNode("Price/LandedPrice/Amount").InnerText;
                if (OtherSellerSubcondition.ToLower().Equals(subcondition.ToLower()))
                {
                    lowestPrice = lowestPrice >= Convert.ToDouble(landedprice, CultureInfo.InvariantCulture) ? Convert.ToDouble(landedprice, CultureInfo.InvariantCulture) : lowestPrice;
                }
            }
            return lowestPrice;
        }

        public static double highestPriceBasedonSubcondition(XmlDocument xml, string subcondition)
        {
            double highest = -1;
            var listings = xml.SelectNodes("//LowestOfferListing");
            string OtherSellerSubcondition = "not found";
            foreach (XmlNode listing in listings)
            {
                OtherSellerSubcondition = listing.SelectSingleNode("Qualifiers/ItemSubcondition").InnerText.ToLower();
                string landedprice = listing.SelectSingleNode("Price/LandedPrice/Amount").InnerText;
                if (OtherSellerSubcondition.ToLower().Equals(subcondition.ToLower()))
                {
                    highest = highest < Convert.ToDouble(landedprice, CultureInfo.InvariantCulture) ? Convert.ToDouble(landedprice, CultureInfo.InvariantCulture) : highest;
                }
            }
            return highest;
        }
        public static Product RepriceThisProduct(Product p)
        {
            XmlDocument xml = new XmlDocument();
            AmazonDetailsGetter detailsgetter = new AmazonDetailsGetter(); ;
            IMWSResponse response;
            //if throttling occurs
            do
            {
                response = detailsgetter.InvokeGetLowestOfferListingsForASIN(p.ASIN, "used");

                if (response.ToXML().ToString().Contains("throttle"))
                {
                    Console.WriteLine("Sleeping for 15 seconds. Throttled");
                    System.Threading.Thread.Sleep(15000);
                }
            } while (response.ToXML().ToString().Contains("throttle"));

            xml.LoadXml(RemoveAllXmlNamespace(response.ToXML()));
            //  File.WriteAllText(@"D:\sss.txt", response.ToXML().ToString());
            p.AcceptablePrice = lowestPriceBasedonSubcondition(xml, "acceptable");
            p.GoodPrice = lowestPriceBasedonSubcondition(xml, "good");
            p.VeryGoodPrice = lowestPriceBasedonSubcondition(xml, "verygood");
            p.MintPrice = lowestPriceBasedonSubcondition(xml, "mint");

            p.HighestGoodPrice = highestPriceBasedonSubcondition(xml, "good");
            p.HighestVeryGoodPrice = highestPriceBasedonSubcondition(xml, "verygood");

            // p.NewPrice = n.GetDocumentBySku(p.SKU).price;

            //  Console.WriteLine(p.GoodPrice);
            //  Console.Read();
            return p;
        }

        //public static Product RepriceThisNewProduct(Product p)
        //{
        //    XmlDocument xml = new XmlDocument();
        //    AmazonDetailsGetter detailsgetter = new AmazonDetailsGetter(); ;
        //    IMWSResponse response;
        //    //if throttling occurs
        //    do
        //    {
        //        response = detailsgetter.InvokeGetLowestOfferListingsForASIN(p.ASIN, "new");

        //        if (response.ToXML().ToString().Contains("throttle"))
        //        {
        //            Console.WriteLine("Sleeping for 15 seconds. Throttled");
        //            System.Threading.Thread.Sleep(15000);
        //        }
        //    } while (response.ToXML().ToString().Contains("throttle"));

        //    xml.LoadXml(RemoveAllXmlNamespace(response.ToXML()));
        //    p.NewPrice = lowestPriceBasedonSubcondition(xml, "new");
        //    p.HighestNewPrice = highestPriceBasedonSubcondition(xml, "new");
        //    return p;
        //}
        private static string RemoveAllXmlNamespace(string xmlData)
        {
            string xmlnsPattern = "\\s+xmlns\\s*(:\\w)?\\s*=\\s*\\\"(?<url>[^\\\"]*)\\\"";
            MatchCollection mathcol = Regex.Matches(xmlData, xmlnsPattern);
            foreach (Match m in mathcol)
            {
                xmlData = xmlData.Replace(m.ToString(), "");
            }
            return xmlData;
        }


    }
}