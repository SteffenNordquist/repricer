﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketplaceWebService;
using MarketplaceWebService.Mock;
using MarketplaceWebService.Model;
using System.Threading;

namespace Repricer
{
    class AmazonUpdater
    {

        private string xmlFolder;
        private string merchantId;
        private string marketplaceId;
        private MarketplaceWebService.MarketplaceWebService service;
        private List<string> logs = new List<string>();
        public AmazonUpdater(string merchantid, string marketplaceid,string accesskeyid,string secretaccesskeyid) {
            logs.Add("Starting Amazon Updater");
            String accessKeyId = accesskeyid;
            String secretAccessKey = secretaccesskeyid;
            xmlFolder = Properties.Settings.Default.XMLFolder;
            
            string applicationName = Properties.Settings.Default.applicationName;
            string applicationVersion = Properties.Settings.Default.applicationVersion;

            merchantId =merchantid;
            marketplaceId = marketplaceId;

            MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
            
            //Germany:
            config.ServiceURL = "https://mws.amazonservices.de";

            config.SetUserAgentHeader(
                applicationName,
                applicationVersion,
                "C#",
                "<Parameter 1>", "<Parameter 2>");
            service = new MarketplaceWebServiceClient(accessKeyId, secretAccessKey, config);

        }

        public void downloadInventoryFile() {
            FileInfo inventoryReportFileInfo = CreateNessecaryFoldersIfNotThere(xmlFolder);
            this.DownloadOpenListingsIfOld(merchantId, marketplaceId, service, inventoryReportFileInfo);
		
        }

        private  static FileInfo CreateNessecaryFoldersIfNotThere(string xmlFolder)
        {
            FileInfo inventoryReportFileInfo = new FileInfo(Properties.Settings.Default.InventoryReport);
            if (!inventoryReportFileInfo.Directory.Exists)
            {
                inventoryReportFileInfo.Directory.Create();
            }
            DirectoryInfo xmlDirectory = new DirectoryInfo(xmlFolder);
            if (!xmlDirectory.Exists)
            {
                xmlDirectory.Create();
            }
            return inventoryReportFileInfo;
        }


        private void DownloadOpenListingsIfOld(string merchantId, string marketplaceId, MarketplaceWebService.MarketplaceWebService service, FileInfo inventoryReportFileInfo)
        {
            bool lastInventoryFileFromAmaIsOld = inventoryReportFileInfo.LastWriteTime.AddDays(1).AddHours(-12) < DateTime.Now;
            if (lastInventoryFileFromAmaIsOld)
            {

                string lastInventoryReportId = this.RequestLatestListingsID(merchantId, service);
                string latestInventoryReportId = "";
                Console.WriteLine("DownloadFile");

                latestInventoryReportId = this.WaitForNewInventoryFileToBeAvailable(merchantId, marketplaceId, service, lastInventoryReportId, latestInventoryReportId);

                this.RequestAndDownloadOpenListings(merchantId, service, latestInventoryReportId);
                Console.WriteLine("DownloadFile - Done");
            }
        }

        private void RequestAndDownloadOpenListings(string merchantId, MarketplaceWebService.MarketplaceWebService service, string latestInventoryReportId)
        {
            GetReportRequest getReportRequest = new GetReportRequest();
            getReportRequest.Merchant = merchantId;

            getReportRequest.ReportId = latestInventoryReportId;
            getReportRequest.Report = File.Open(Properties.Settings.Default.InventoryReport, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            GetReportSample.InvokeGetReport(service, getReportRequest);

            getReportRequest.Report.Close();
        }


        private void RequestOpenListings(string merchantId, string marketplaceId, MarketplaceWebService.MarketplaceWebService service)
        {
            RequestReportRequest request = new RequestReportRequest();
            request.Merchant = merchantId;
            request.MarketplaceIdList = new IdList();
            request.MarketplaceIdList.Id = new List<string>(new string[] { marketplaceId });

            request.ReportType = "_GET_FLAT_FILE_OPEN_LISTINGS_DATA_";
            // @TODO: set additional request parameters here
            request.ReportOptions = "ShowSalesChannel=true";
            RequestReportSample.InvokeRequestReport(service, request);
        }



        private string WaitForNewInventoryFileToBeAvailable(string merchantId, string marketplaceId, MarketplaceWebService.MarketplaceWebService service, string lastInventoryReportId, string latestInventoryReportId)
        {
            RequestOpenListings(merchantId, marketplaceId, service);
            for (int i = 0; i < 15; i++)
            {
                System.Threading.Thread.Sleep(1000 * 60 * 3); // 3 Minutes
                latestInventoryReportId = RequestLatestListingsID(merchantId, service);
                if (latestInventoryReportId != lastInventoryReportId)
                {
                    i = 999;
                }
            }
            return latestInventoryReportId;
        }

        private string RequestLatestListingsID(string merchantId, MarketplaceWebService.MarketplaceWebService service)
        {
            string latestInventoryReportId = "";

            GetReportListRequest getReportListRequest = new GetReportListRequest();
            getReportListRequest.Merchant = merchantId;
            getReportListRequest.AvailableFromDate = DateTime.Now.AddDays(-2);


            latestInventoryReportId = GetReportListSample.GetLatestInventoryReportID(service, getReportListRequest);
            return latestInventoryReportId;
        }

        public void SendFeeds()
        {
            logs.Add("Sending Feeds");

            SendFeeds(xmlFolder,merchantId,marketplaceId,service);
        }

        public string getLogs() {
            string tmp = "";
            foreach (string log in logs) {
                tmp += log + "\n"; 
            }
            return tmp;
        }
        
        private void SendFeeds(string xmlFolder, string merchantId, string marketplaceId, MarketplaceWebService.MarketplaceWebService service)
        {
            Console.WriteLine("SendFeeds");
            logs.Add("starting to send feed");
            List<SubmitFeedResponse> responseListPrice = new List<SubmitFeedResponse>();
            {

                DirectoryInfo di = new DirectoryInfo(xmlFolder);
                FileInfo[] fi = di.GetFiles();

                int productCount = 0, inventoryCount = 0, priceCount = 0;

                foreach (FileInfo f in fi)
                {
                    if (f.Name.StartsWith("BookPrice"))
                        priceCount++;
                }

                int max = Math.Max(Math.Max(productCount, priceCount), inventoryCount);
                for (int i = 1; i <= max; i++)
                {

                    if (i <= priceCount)
                    {
                        SubmitFeedResponse smfrPrice = null;
                        while (smfrPrice == null)
                        {
                            logs.Add("Submitting Price"+i+".XML");

                            smfrPrice = SubmitFeed(merchantId, marketplaceId, service, i, xmlFolder + @"\BookPrice{0}.xml", "_POST_PRODUCT_PRICING_DATA_");
                        }
                        responseListPrice.Add(smfrPrice);
                    }

                    if (i != max) // last - no sleep
                    {
                        Thread.Sleep(1000 * 60 * 10);
                    }
                }
                Console.WriteLine("SendFeeds - Done");
            }

         
            foreach (var response in responseListPrice)
            {
                File.AppendAllText(xmlFolder + "\\PriceResponse.xml", response.ToXML() + "\r\n");
            }
        }

        private SubmitFeedResponse SubmitFeed(string merchantId, string marketplaceId, MarketplaceWebService.MarketplaceWebService service, int i, string fileName, string feedType)
        {
            SubmitFeedRequest request = new SubmitFeedRequest();
            request.Merchant = merchantId;
            request.MarketplaceIdList = new IdList();
            request.MarketplaceIdList.Id = new List<string>(new string[] { marketplaceId });

            string fullFile = string.Format(fileName, i);
            request.FeedContent = File.Open(fullFile, FileMode.Open, FileAccess.Read);

            request.ContentMD5 = MarketplaceWebServiceClient.CalculateContentMD5(request.FeedContent);
            request.FeedContent.Position = 0;
            request.FeedType = feedType;
            SubmitFeedResponse response = SubmitFeedSample.InvokeSubmitFeed(service, request);
            request.FeedContent.Close();
            return response;
        }
    }
}
