﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Repricer
{
    class Product
    {
        //from the inventory file
        private string sku;
        private string asin;


        private double originalLandedPrice;

        //from amazon
        private string subcondition;

        //from amazon, based on the sku
        private double otherSellerPrice;
        private string otherSellerSubCondition;

        //for the computation
        private double newPrice = 0;
        private double acceptablePrice = 0;
        private double goodPrice = 0;
        private double verygoodPrice = 0;
        private double mintPrice = 0;

        private bool PlusTwentyPercent = false;
        private double highestVeryGoodPrice = 0;
        private double highestGoodPrice = 0;
        private double highestNewPrice = 0;


        //
        private bool twentyPercentRule = false;
        //for reporting purposes
        private string remarks;
        public double NewPrice
        {
            get
            {
                return this.newPrice;
            }
            set
            {
                this.newPrice = value;
            }
        }

        public double AcceptablePrice
        {
            get
            {
                return this.acceptablePrice;
            }
            set
            {
                this.acceptablePrice = value;

            }
        }
        public double GoodPrice
        {
            get
            {
                return this.goodPrice;
            }
            set
            {
                this.goodPrice = value;

            }
        }
        public double VeryGoodPrice
        {
            get
            {
                return this.verygoodPrice;
            }
            set
            {
                this.verygoodPrice = value;

            }
        }
        public double MintPrice
        {
            get
            {
                return this.mintPrice;
            }
            set
            {
                this.mintPrice = value;

            }
        }

        public double HighestVeryGoodPrice
        {
            get
            {
                return this.highestVeryGoodPrice;
            }
            set
            {
                this.highestVeryGoodPrice = value;

            }
        }

        public double HighestGoodPrice
        {
            get
            {
                return this.highestGoodPrice;
            }
            set
            {
                this.highestGoodPrice = value;

            }
        }
        public double HighestNewPrice
        {
            get
            {
                return this.highestNewPrice;
            }
            set
            {
                this.highestNewPrice = value;

            }
        }

        public Product(string sku, string asin, string subcondition, double listingPrice, bool twentyPercentRule)
        {
            this.sku = sku;
            this.asin = asin;
            this.subcondition = subcondition;
            this.originalLandedPrice = listingPrice + 3;
            this.otherSellerPrice = Double.MaxValue;
            this.remarks = "";
            this.twentyPercentRule = twentyPercentRule;
        }

        public string SKU
        {
            get
            {
                return this.sku;
            }
            set
            {
                this.sku = value;
            }
        }

        public string ASIN
        {
            get
            {
                return this.asin;
            }
            set
            {
                this.asin = value;
            }
        }

        public double OriginalLandedPrice
        {
            get
            {
                return this.originalLandedPrice;
            }
            set
            {
                this.originalLandedPrice = value;
            }
        }


        public string Subcondition
        {
            get
            {
                return this.subcondition;
            }
            set
            {
                this.subcondition = value;
            }
        }

        public double OtherSellerLandedPrice
        {
            get
            {
                return this.otherSellerPrice;
            }
            set
            {
                this.otherSellerPrice = value;
            }
        }

        public string OtherSellerSubcondition
        {
            get
            {
                return this.otherSellerSubCondition;
            }
            set
            {
                this.otherSellerSubCondition = value;
            }
        }

        public string Remarks
        {
            get
            {
                return this.remarks;
            }
            set
            {
                this.remarks = value + " ";
            }
        }

        private void SetSubconditionAndPrice()
        {
            this.PlusTwentyPercent = false;

            double tempLowestPrice;

            if (this.subcondition.ToLower().Equals("new"))
            {
                //if there's no new
                if (newPrice == double.MaxValue)
                {
                    otherSellerSubCondition = "none found";
                    OtherSellerLandedPrice = double.MaxValue;
                }
                else
                {
                    OtherSellerLandedPrice = newPrice;
                    otherSellerSubCondition = "new";
                }

            }


            if (this.subcondition.ToLower().Equals("mint"))
            {
                //if there's no MINT. try VERY GOOD
                if (mintPrice == double.MaxValue)
                {
                    //if there is very good, increase the price

                    if (highestVeryGoodPrice != -1)
                    {
                        otherSellerPrice = highestVeryGoodPrice;
                        otherSellerSubCondition = "verygood";
                        this.PlusTwentyPercent = true;
                    }
                    else
                    {
                        otherSellerSubCondition = "none found";
                        OtherSellerLandedPrice = double.MaxValue;
                    }

                }
                else
                {
                    OtherSellerLandedPrice = mintPrice;
                    otherSellerSubCondition = "mint";
                }
            }



            if (this.subcondition.ToLower().Equals("verygood"))
            {
                tempLowestPrice = verygoodPrice; //lowestPriceBasedonSubcondition(xml, "verygood");
                if (tempLowestPrice != double.MaxValue)
                {
                    this.OtherSellerLandedPrice = tempLowestPrice;
                    this.OtherSellerSubcondition = "verygood";
                }
                else
                {
                    tempLowestPrice = mintPrice;
                    //if there is a mint.
                    if (tempLowestPrice != double.MaxValue)
                    {
                        this.OtherSellerLandedPrice = tempLowestPrice;
                        this.OtherSellerSubcondition = "mint";
                    }
                    //if there's no mint, go down to good
                    else
                    {
                        //if there's a good
                        if (highestGoodPrice != -1)
                        {
                            this.PlusTwentyPercent = true;
                            this.OtherSellerLandedPrice = highestGoodPrice;
                            this.OtherSellerSubcondition = "good";
                        }
                        else
                        {
                            otherSellerSubCondition = "none found";
                            OtherSellerLandedPrice = double.MaxValue;
                        }
                    }

                }

            }


            if (this.Subcondition.ToLower().Equals("good"))
            {
                tempLowestPrice = goodPrice;//lowestPriceBasedonSubcondition(xml, "good");
                if (tempLowestPrice != double.MaxValue)
                {
                    this.OtherSellerLandedPrice = tempLowestPrice;
                    this.OtherSellerSubcondition = "good";
                }
                else
                {
                    tempLowestPrice = verygoodPrice; //lowestPriceBasedonSubcondition(xml, "verygood");
                    if (tempLowestPrice != double.MaxValue)
                    {
                        this.OtherSellerLandedPrice = tempLowestPrice;
                        this.OtherSellerSubcondition = "verygood";
                    }
                }
            }

            if (this.Subcondition.ToLower().Equals("acceptable"))
            {
                tempLowestPrice = this.acceptablePrice; //lowestPriceBasedonSubcondition(xml, "acceptable");
                if (tempLowestPrice != double.MaxValue)
                {
                    this.OtherSellerLandedPrice = tempLowestPrice;
                    this.OtherSellerSubcondition = "acceptable";
                }
                else
                {
                    tempLowestPrice = goodPrice;//lowestPriceBasedonSubcondition(xml, "good");
                    if (tempLowestPrice != double.MaxValue)
                    {
                        this.OtherSellerLandedPrice = tempLowestPrice;
                        this.OtherSellerSubcondition = "good";
                    }
                    else
                    {
                        tempLowestPrice = verygoodPrice; //lowestPriceBasedonSubcondition(xml, "verygood");
                        if (tempLowestPrice != double.MaxValue)
                        {
                            this.OtherSellerLandedPrice = tempLowestPrice;
                            this.OtherSellerSubcondition = "verygood";
                        }
                    }
                }
            }

        }

        public bool MatchRegex(string line)
        {
            Regex regex = new Regex(@"A\d*-.*");
            Match match = regex.Match(line);
            if (match.Success)
                return true;
            else
                return false;
        }

        public double NewListingPrice()
        {
            SetSubconditionAndPrice();
            double lowestPrice = (originalLandedPrice - 3);
            //To prevent Double.MAX_VALUE
            if (this.otherSellerPrice == Double.MaxValue)
            {
                lowestPrice = this.originalLandedPrice - 3;
                this.remarks = "Cant find other seller price. Retain old price ";
            }
            else
            {
                if (this.PlusTwentyPercent)
                {
                    if (this.MatchRegex(this.sku))
                    {
                        lowestPrice = Math.Round(otherSellerPrice - 3.01, 2) >= 0.01 ? Math.Round(otherSellerPrice - 3.01, 2) : 0.01;
                        this.remarks = "Underbidding";
                    }
                    else
                    {
                        lowestPrice = Math.Round((otherSellerPrice + ((otherSellerPrice - 3) * .2)) - 3, 2);
                        this.remarks = "Increasing the price to +20%.";
                    }

                }
                else
                {
                    //If we are cheaper, then increase our price
                    if (this.originalLandedPrice <= this.otherSellerPrice)
                    {
                        if (this.originalLandedPrice == this.otherSellerPrice)
                            this.remarks = "Equal price with other Seller. Decreasing our price by .01. ";
                        else if (this.originalLandedPrice == (this.otherSellerPrice - .01))
                            this.remarks = "We have cheaper by 0.01. Retaining our price ";
                        else
                            this.remarks = "We have cheaper price than other seller. Increasing our price ";
                        lowestPrice = this.otherSellerPrice - 3.01;
                    }
                    //if the other seller is cheaper, then do something else.
                    else
                    {

                        if (twentyPercentRule)//Properties.Settings.Default.TwentyPercentRule)
                            lowestPrice = this.PriceAfterTwentyPercent(this.otherSellerPrice, this.originalLandedPrice);
                        else
                        {
                            this.remarks = "Twenty percent rule is FALSE. Using OLD PRICE";
                            lowestPrice = originalLandedPrice;
                        }
                        if (!twentyPercentRule && (this.sku.StartsWith("J-")))
                        {
                            this.remarks = "J- and G- are Hardcore! we will underbid";
                            lowestPrice = otherSellerPrice - 3.01;
                            if (lowestPrice <= 0)
                                lowestPrice = 0.01;
                        }

                    }



                }
            }
            return lowestPrice;
        }

        private double PriceAfterTwentyPercent(double otherSellerLandedPrice, double currentLandedPrice)
        {
            //Try to lower our price to 20% lower 
            //              (currentLandedPrice * .8) - 3)
            //check if it is lower than the other sellers Price

            if (this.MatchRegex(this.sku))
            {
            
                //if (((currentLandedPrice / 2) - 3) <= (otherSellerLandedPrice - 3))
                //{
                //    this.remarks = "We are more expensive than the other Seller. Underbidding ";
                //    //if after lowering our price by 20%, we are CHEAPER, then we will follow the other sellers price -.01
                //    return (otherSellerLandedPrice - 3.01) >= 0.01 ? (otherSellerLandedPrice - 3.01) : 0.01;
                //}
                //else
                //{
                //    this.remarks = "Even after lowering our price to 50% we are still more expensive. Retaining our original price";
                //    //if after lowering our price by 20%, we are EXPENSIVE, then we will not compete with the other seller.    
                //    return (currentLandedPrice - 3);
                //}
                
                return (otherSellerLandedPrice - 3.01) >= 0.01 ? (otherSellerLandedPrice - 3.01) : 0.01;
            
                
            }
            else
            {
                
                //if (((currentLandedPrice * .8) - 3) <= (otherSellerLandedPrice - 3))
                //{
                //    this.remarks = "We are more expensive than the other Seller. Repricing ";
                //    //if after lowering our price by 20%, we are CHEAPER, then we will follow the other sellers price -.01
                //    return (otherSellerLandedPrice - 3.01) >= 0.01 ? (otherSellerLandedPrice - 3.01) : 0.01;
                //}
                //else
                //{
                //    this.remarks = "Even after lowering our price to 20% we are still more expensive. Retaining our original price";
                //    //if after lowering our price by 20%, we are EXPENSIVE, then we will not compete with the other seller.    
                //    return (currentLandedPrice - 3);
                //}
                    return (otherSellerLandedPrice - 3.01) >= 0.01 ? (otherSellerLandedPrice - 3.01) : 0.01;
           
            }

        }


    }
}
