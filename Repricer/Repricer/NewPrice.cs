﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repricer
{
    class NewPrice
    {
        public ObjectId id { get; set; }

        public string sku { get; set; }
        public string asin { get; set; }
        public double price { get; set; }
    }
}
