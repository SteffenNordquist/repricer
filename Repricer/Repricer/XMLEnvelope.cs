﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Repricer
{
    class XMLEnvelope
    {
        private XmlDocument doc;
        private XmlNode amazonEnvelope;
        private long timeStamp = DateTime.Now.Ticks;

        public XMLEnvelope()
        {

        }

        public void AddHeader()
        {
            long timeStamp = DateTime.Now.Ticks;
            FileInfo xmlfile = new FileInfo(Properties.Settings.Default.UpdatedPrice);

            doc = new XmlDocument();

            XmlElement el = doc.CreateElement(string.Empty, "AmazonEnvelope", string.Empty);
            doc.AppendChild(el);
            doc.Save(Properties.Settings.Default.UpdatedPrice);

            doc.Load(Properties.Settings.Default.UpdatedPrice);
            XmlDeclaration xmldecl;
            xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            xmldecl.Encoding = "UTF-8";
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmldecl, root);

            XmlText text;

            amazonEnvelope = doc.SelectSingleNode("//AmazonEnvelope");

            XmlNode header = doc.CreateElement(string.Empty, "Header", string.Empty);
            amazonEnvelope.AppendChild(header);

            XmlNode docVersion = doc.CreateElement(string.Empty, "DocumentVersion", string.Empty);
            header.AppendChild(docVersion);
            text = doc.CreateTextNode("1.01");
            docVersion.AppendChild(text);

            XmlNode MerchantIdentifier = doc.CreateElement(string.Empty, "MerchantIdentifier", string.Empty);
            header.AppendChild(MerchantIdentifier);
            text = doc.CreateTextNode(Properties.Settings.Default.merchantId);
            MerchantIdentifier.AppendChild(text);

            XmlNode MessageType = doc.CreateElement(string.Empty, "MessageType", string.Empty);
            amazonEnvelope.AppendChild(MessageType);
            text = doc.CreateTextNode("Price");
            MessageType.AppendChild(text);


        }

        public void AddNewNode(string sku, double lowestprice)
        {
            if (lowestprice > 0) {
                XmlText text;
                XmlNode Message = doc.CreateElement(string.Empty, "Message", string.Empty);
                amazonEnvelope.AppendChild(Message);

                XmlNode MessageID = doc.CreateElement(string.Empty, "MessageID", string.Empty);
                Message.AppendChild(MessageID);
                text = doc.CreateTextNode(timeStamp++.ToString());
                MessageID.AppendChild(text);

                XmlNode Price = doc.CreateElement(string.Empty, "Price", string.Empty);
                Message.AppendChild(Price);


                XmlNode SKU = doc.CreateElement(string.Empty, "SKU", string.Empty);
                Price.AppendChild(SKU);
                text = doc.CreateTextNode(sku);
                SKU.AppendChild(text);

                XmlElement StandardPrice = doc.CreateElement(string.Empty, "StandardPrice", string.Empty);
                StandardPrice.SetAttribute("currency", "EUR");

                Price.AppendChild(StandardPrice);
                text = doc.CreateTextNode(lowestprice.ToString("0.00", CultureInfo.InvariantCulture));
                StandardPrice.AppendChild(text);
            } else { 
            }
          
        }

        public void SaveXML() {
            doc.Save(Properties.Settings.Default.UpdatedPrice);
          
        }
    }
}
